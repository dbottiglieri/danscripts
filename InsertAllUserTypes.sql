DECLARE @UserName NVarChar(200);
DECLARE @Role NVarChar(200);
DECLARE @Culture NVARCHAR(6);

SET @Culture = 'en-GB';

SET @Role = 'Admin';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(LOWER(@UserName)), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(LOWER(@UserName)), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @UserName = 'dbottiglieri@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(LOWER(@UserName)), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', 'Dan Admin')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(LOWER(@UserName)), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @Role = 'Standard';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(@UserName), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(@UserName), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @UserName = 'daniel.bottiglieri@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(LOWER(@UserName)), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', 'Dan Standard')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(LOWER(@UserName)), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @Role = 'Uploader';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(@UserName), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(@UserName), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @Role = 'UploaderPublisher';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(@UserName), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(@UserName), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @Role = 'Privileged';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(@UserName), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(@UserName), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END

SET @Role = 'Guest';
SET @UserName = @Role + '@adgistics.com';
IF ((SELECT COUNT(*) FROM [Adgistics_User] WHERE [Username] = LOWER(@UserName)) = 0)
BEGIN
	IF (EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND  TABLE_NAME = 'Adgistics_User'))  BEGIN
	 IF not exists(select * from [dbo].[Adgistics_User] where Username = LOWER(@UserName)) BEGIN
	  INSERT INTO [dbo].[Adgistics_User] ([Username] ,[Attempts] ,[Status] ,[PasswordHash] ,[UserGuid]) VALUES (LOWER(@UserName), 0, 1, '+gQWnPXQWFGx2UIB+Vo+SWtzlWkxyPQM:iEV8rOFLVpSWRrrSEmATbfOH0as+tveA', 'e9fd71e5-f8cc-476a-9aaf-b95d573ed743')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Email', LOWER(@UserName))
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), '__Culture', @Culture)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'FirstName', @Role)
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'Surname', 'User')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'SSOUser', 'FALSE')
	  INSERT INTO [dbo].[Adgistics_UserProfile] ([Username], [Key], [Value]) VALUES (LOWER(@UserName), 'DefaultSite', 'www')	        
		UPDATE Adgistics_User SET [Status] = 1 WHERE Username = LOWER(@UserName) ;
		INSERT INTO Adgistics_UserRole (Username, [Role])
		SELECT LOWER(@UserName), Id
		FROM Adgistics_Role
		WHERE Name like '%' + @Role;
	   END
	END
END