IF EXIST dandev.7z DEL /F dandev.7z
cd Terremoto/build/src
7z a ../../../dandev *.exe *.dll *.config *.html *.log *.xml *.pdb
cd ../../..
IF EXIST \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev RMDIR /S /Q \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev
MOVE dandev.7z \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\
7z x \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev.7z -o\\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev
DEL /F \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev.7z
IF %1.==. GOTO TheEnd
IF EXIST \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\%1 RMDIR /S /Q \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\%1
MOVE \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\dandev \\buildsrv3\Build\releases\Adgistics.Terremoto\archive\beta\%1
:TheEnd
