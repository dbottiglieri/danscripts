IF %1.==. GOTO BuildAll
for /f "delims=" %%a in ("%1") do set "fname=%%~nxa"
msbuild Terremoto\%1\%fname%.csproj /p:notest=true

for /r %%s in (Terremoto\%1\bin\debug\*.dll* Terremoto\%1\bin\debug\*.pdb) do xcopy /y "%%s" Terremoto\build\src /i
GOTO TheEnd

:BuildAll
msbuild Terremoto\build.proj /p:notest=true

:TheEnd
@echo Finished: %time%